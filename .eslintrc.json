{
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "extends": [
    "airbnb"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "@babel/eslint-parser",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "plugins": [
    "react"
  ],
  "settings": {
    "import/core-modules": ["electron"]
  },
  "rules": {
    "array-bracket-newline": ["error", "consistent"],
    "array-bracket-spacing": ["error", "never"],
    "arrow-parens": [2, "as-needed"],
    "block-spacing": ["error", "always"],
    "brace-style": ["error", "1tbs"],
    "camelcase": ["error", { "properties": "always" }],
    "capitalized-comments": ["error", "always"],
    "class-methods-use-this": [0],
    "comma-dangle": ["error", {
      "arrays": "always-multiline",
      "objects": "always-multiline",
      "imports": "always-multiline",
      "exports": "always-multiline",
      "functions": "never"
    }],
    "comma-spacing": ["error", { "before": false, "after": true }],
    "comma-style": ["error", "last"],
    "computed-property-spacing": ["error", "never"],
    "consistent-return": [0],
    "eol-last": ["error", "always"],
    "func-call-spacing": ["error", "never"],
    "func-names": ["error", "always"],
    "function-call-argument-newline": ["error", "consistent"],
    "function-paren-newline": ["error", "consistent"],
    "implicit-arrow-linebreak": ["error", "beside"],
    "import/prefer-default-export": [0],
    "indent": ["error", 2, { "SwitchCase": 1, "ignoredNodes": ["JSXElement"] }],
    "jsx-quotes": ["error", "prefer-double"],
    "key-spacing": ["error", { "beforeColon": false, "afterColon": true, "mode": "strict" }],
    "keyword-spacing": ["error", { "before": true, "after": true }],
    "lines-around-comment": ["error", {
      "beforeBlockComment": true,
      "afterBlockComment": false,
      "beforeLineComment": true,
      "afterLineComment": false,
      "allowBlockStart": true,
      "allowBlockEnd": true,
      "allowObjectStart": true,
      "allowObjectEnd": true,
      "allowArrayStart": true,
      "allowArrayEnd": true,
      "allowClassStart": true,
      "allowClassEnd": true
    }],
    "max-len": ["error", {
      "code": 80,
      "tabWidth": 2,
      "comments": 80,
      "ignoreUrls": true,
      "ignoreRegExpLiterals": true
    }],
    "max-lines": ["warn", 300],
    "multiline-comment-style": ["error", "starred-block"],
    "multiline-ternary": ["error", "always-multiline"],
    "new-parens": ["error", "always"],
    "newline-per-chained-call": ["error", { "ignoreChainWithDepth": 2 }],
    "no-array-constructor": ["error"],
    "no-else-return": ["error", { "allowElseIf": true }],
    "no-floating-decimal": [0],
    "no-mixed-operators": ["error"],
    "no-multi-assign": ["error"],
    "no-multiple-empty-lines": ["error", { "max": 3, "maxEOF": 1, "maxBOF": 0 }],
    "no-nested-ternary": [0],
    "no-new-object": ["error"],
    "no-param-reassign": ["error", { "props": false }],
    "no-plusplus": [0],
    "no-tabs": ["error"],
    "no-trailing-spaces": ["error"],
    "no-use-before-define": ["error", { "functions": false, "classes": true, "variables": true }],
    "no-whitespace-before-property": ["error"],
    "nonblock-statement-body-position": ["error", "beside"],
    "object-curly-newline": ["error", { "consistent": true }],
    "object-curly-spacing": ["error", "always"],
    "one-var": ["error", "never"],
    "one-var-declaration-per-line": ["error", "always"],
    "operator-assignment": ["error", "always"],
    "operator-linebreak": [
      "error",
      "before",
      {
        "overrides": {
          "=": "after",
          "+=": "after",
          "-=": "after",
          "*=": "after",
          "/=": "after",
          "?": "after",
          ":": "after"
        }
      }
    ],
    "padded-blocks": ["error", "never"],
    "padding-line-between-statements": [
      "error",
      { "blankLine": "always", "prev": ["const", "let", "var"], "next": "*" },
      { "blankLine": "any", "prev": ["const", "let", "var"], "next": ["const", "let", "var"] },
      { "blankLine": "always", "prev": "*", "next": "return" },
      { "blankLine": "always", "prev": "import", "next": "*" },
      { "blankLine": "any", "prev": "import", "next": "import" }
    ],
    "prefer-object-spread": ["error"],
    "quote-props": ["error", "as-needed"],
    "quotes": ["error", "single"],
    "semi": ["error", "always"],
    "semi-spacing": ["error", { "before": false, "after": true }],
    "semi-style": ["error", "last"],
    "space-before-function-paren": ["error", "never"],
    "space-in-parens": ["error", "never"],
    "space-infix-ops": ["error"],
    "spaced-comment": ["error", "always"],
    "switch-colon-spacing": ["error", { "before": false, "after": true }],
    "template-curly-spacing": ["error", "always"],
    "jsx-a11y/alt-text": [0],
    "jsx-a11y/click-events-have-key-events": ["warn"],
    "jsx-a11y/label-has-associated-control": ["error", {
      "assert": "either"
    }],
    "jsx-a11y/no-static-element-interactions": ["warn"],
    "react/button-has-type": ["error", {
      "button": true,
      "submit": true,
      "reset": true
    }],
    "react/function-component-definition": [0],
    "react/jsx-closing-tag-location": [0],
    "react/jsx-curly-spacing": [
      "error",
      "always",
      {
        "allowMultiline": true,
        "spacing": {
          "objectLiterals": "never"
        }
      }
    ],
    "react/jsx-one-expression-per-line": [0],
    "react/jsx-wrap-multilines": ["error", {
      "declaration": "parens",
      "assignment": "parens",
      "return": "parens",
      "arrow": "parens",
      "condition": "ignore",
      "logical": "ignore",
      "prop": "ignore"
    }],
    "react/prop-types": [0],
    "react/no-array-index-key": [0]
  }
}
