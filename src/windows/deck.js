import { BrowserWindow, Menu, app, ipcMain } from 'electron';

import loadColumn from '../webviews/column';
import loadPreview from '../webviews/preview';

class Deck {
  constructor(config) {
    const position = config.getPosition();

    this.config = config;
    this.columns = config.getColumns();
    this.elements = {
      win: new BrowserWindow({
        title: 'Deck',
        fullscreen: false,
        transparent: false,
        frame: true,
        alwaysOnTop: false,
        show: false,
        backgroundColor: '#222',
        darkTheme: true,
        x: position.left,
        y: position.top,
        height: position.height,
        width: position.width,
        webPreferences: {
          nodeIntegration: true,
          contextIsolation: true,
        },
      }),
      devtools: null,
      columns: [],
      preview: null,
      previewCross: null,
    };


    /**
     * Events
     */
    this.elements.win.on('show', () => {
      if (position.maximized) {
        this.elements.win.maximize();
      }

      this.resize();
    });
    this.elements.win.on('moved', () => this.resize());
    this.elements.win.on('resize', () => this.resize());
    this.elements.win.on('maximize', () => this.resize());
    this.elements.win.on('unmaximize', () => this.resize());
    this.elements.win.on('minimize', () => this.resize());
    this.elements.win.on('restore', () => this.resize());
    this.elements.win.on('enter-full-screen', () => this.resize());
    this.elements.win.on('leave-full-screen', () => this.resize());
    this.elements.win.on('enter-html-full-screen', () => this.resize());
    this.elements.win.on('leave-html-full-screen', () => this.resize());
    this.elements.win.on('close', () => {
      this.closeWindow();
    });
    ipcMain.on('closePreview', () => this.closePreview());


    this.makeContextMenu();
    this.elements.win.show();
    this.loadColumns();
  }

  makeContextMenu() {
    const template = [
      {
        label: 'File',
        submenu: [
          {
            label: 'Reload columns',
            click: () => {
              this.closePreview();
              this.loadColumns();
            },
          },
          {
            label: 'Restart',
            click: () => {
              app.relaunch({ execPath: process.env.PORTABLE_EXECUTABLE_FILE });
              app.exit(0);
            },
          },
          { role: 'quit' },
        ],
      },
      {
        label: 'Tools',
        submenu: this.columns.map((col, i) => ({
          label: `Column ${ i + 1 } DevTools`,
          click: () => this.toggleDevtools(i),
        })),
      },
    ];

    this.elements.win.setMenu(Menu.buildFromTemplate(template));
  }

  loadColumns() {
    this.elements.columns.forEach(view => {
      view.webContents.destroy();
      this.elements.win.removeBrowserView(view);
    });
    this.elements.columns = [];
    this.columns.forEach(column => {
      const view = loadColumn(column, this.openPreview.bind(this));

      this.elements.columns.push(view);
      this.elements.win.addBrowserView(view);
    });
    this.resizeColumns();
  }

  openPreview(link) {
    const { preview, cross } = loadPreview(link);

    this.elements.preview = preview;
    this.elements.previewCross = cross;
    this.elements.win.addBrowserView(preview);
    this.elements.win.addBrowserView(cross);
    this.resizePreview();
  }

  closePreview() {
    if (this.elements.preview) {
      this.elements.preview.webContents.destroy();
      this.elements.preview = null;
    }

    if (this.elements.previewCross) {
      this.elements.previewCross.webContents.destroy();
      this.elements.previewCross = null;
    }
  }

  resizeColumns() {
    const [width, height] = this.elements.win.getContentSize();
    const columnWidth = Math.round(width / this.columns.length);

    this.elements.columns.forEach((column, i) => {
      column.setBounds({
        x: i * columnWidth,
        y: 0,
        width: columnWidth,
        height,
      });
    });
  }

  resizePreview() {
    if (!this.elements.preview) return;

    const [width, height] = this.elements.win.getContentSize();

    this.elements.preview.setBounds({
      x: 0,
      y: 0,
      width,
      height,
    });
    this.elements.previewCross.setBounds({
      x: width - 50,
      y: 0,
      width: 50,
      height: 50,
    });
  }

  savePosition() {
    const [x, y] = this.elements.win.getPosition();
    const [width, height] = this.elements.win.getSize();
    const isMaximized = this.elements.win.isMaximized();

    this.config.saveConfig({
      key: 'position',
      value: {
        top: y,
        left: x,
        height,
        width,
        maximized: isMaximized,
      },
    });
  }

  resize() {
    this.resizeColumns();
    this.resizePreview();
    this.savePosition();
  }

  toggleDevtools(index) {
    const el = this.elements.columns[index];

    if (this.elements.devtools && !this.elements.devtools.isDestroyed()) {
      this.elements.devtools.destroy();
      this.elements.devtools = null;
    }

    if (el) {
      this.elements.devtools = new BrowserWindow({
        title: `Dev Tools - Column ${ index + 1 }`,
        fullscreen: false,
        transparent: false,
        frame: true,
        alwaysOnTop: false,
        x: 2000,
        y: 0,
        height: 1000,
        width: 1500,
        show: true,
        webPreferences: {
          nodeIntegration: true,
        },
      });

      el.webContents.setDevToolsWebContents(this.elements.devtools.webContents);
      el.webContents.openDevTools({ mode: 'detach' });

      this.elements.devtools.on('close', () => {
        this.elements.devtools.destroy();
        this.elements.devtools = null;
      });
    }
  }

  closeWindow() {
    if (this.elements.devtools && !this.elements.devtools.isDestroyed()) {
      this.elements.devtools.destroy();
    }

    if (this.elements.preview) {
      this.elements.preview.webContents.destroy();
    }

    if (this.elements.previewCross) {
      this.elements.previewCross.webContents.destroy();
    }

    if (this.elements.win && !this.elements.win.isDestroyed()) {
      this.elements.columns.forEach(view => {
        if (view) {
          view.webContents.destroy();
        }
      });

      this.elements.win.destroy();
    }

    this.elements = null;
  }
}

export default Deck;
