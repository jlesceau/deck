import fetch from 'node-fetch';
import { app, session } from 'electron';
import { ElectronBlocker } from '@cliqz/adblocker-electron';

import Deck from './windows/deck';
import Config from './config/config';

async function start() {
  const config = new Config();

  await config.loadConfig();

  try {
    const adblocker = await ElectronBlocker
      .fromPrebuiltAdsAndTracking(fetch);

    adblocker.enableBlockingInSession(session.defaultSession);
  } catch (ex) {} // eslint-disable-line

  const deck = new Deck(config);
}

function close() {
}

app.commandLine.appendSwitch('disable-features', 'HardwareMediaKeyHandling');

// Modify the display name on Windows
if (process.platform === 'win32') {
  app.setAppUserModelId('Deck');
}

// Make sure it's the first and only instance or quit
if (!app.requestSingleInstanceLock()) {
  app.quit();
}

app.on('ready', start);
app.on('will-quit', close);
