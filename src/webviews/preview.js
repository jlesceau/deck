import { join } from 'path';
import { BrowserView, shell } from 'electron';

export default function loadView(url) {
  const preview = new BrowserView({
    webPreferences: {
      disableHtmlFullscreenWindowResize: true,
    },
  });

  preview.webContents.setWindowOpenHandler(details => {
    shell.openExternal(details.url);

    return { action: 'deny' };
  });

  preview.webContents.loadURL(
    url,
    {
      userAgent: [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
        'AppleWebKit/537.36 (KHTML, like Gecko)',
        `Chrome/${ process.versions.chrome }`,
        'Safari/537.36',
      ].join(' '),
    }
  );

  const cross = new BrowserView({
    webPreferences: {
      preload: join(__dirname, 'scripts/preload.js'),
    },
  });

  cross.webContents.loadFile(join(__dirname, 'html/cross.html'));

  return { preview, cross };
}
