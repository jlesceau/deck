import { readFileSync } from 'fs';
import { BrowserView, shell } from 'electron';

const scripts = {
  home: {
    js: readFileSync(`${ __dirname }/scripts/home.js`, 'utf8'),
    css: readFileSync(`${ __dirname }/scripts/home.css`, 'utf8'),
  },
  list: {
    js: readFileSync(`${ __dirname }/scripts/list.js`, 'utf8'),
    css: readFileSync(`${ __dirname }/scripts/list.css`, 'utf8'),
  },
  search: {
    js: readFileSync(`${ __dirname }/scripts/search.js`, 'utf8'),
    css: readFileSync(`${ __dirname }/scripts/search.css`, 'utf8'),
  },
  trends: {
    css: readFileSync(`${ __dirname }/scripts/trends.css`, 'utf8'),
  },
};

export default function loadView({ type, url }, openPreview) {
  const view = new BrowserView({
    webPreferences: {
      disableHtmlFullscreenWindowResize: true,
    },
  });

  view.webContents.on('dom-ready', () => {
    if (scripts[type]?.js) {
      view.webContents.executeJavaScript(scripts[type].js);
    }

    if (scripts[type]?.css) {
      view.webContents.insertCSS(scripts[type].css);
    }
  });

  view.webContents.setWindowOpenHandler(details => {
    if (details.url.startsWith('/')) {
      openPreview(`https://twitter.com${ details.url }`);
    } else if (details.url.startsWith('https://twitter.com')) {
      openPreview(details.url);
    } else {
      shell.openExternal(details.url);
    }

    return { action: 'deny' };
  });

  view.webContents.loadURL(
    url,
    {
      userAgent: [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
        'AppleWebKit/537.36 (KHTML, like Gecko)',
        `Chrome/${ process.versions.chrome }`,
        'Safari/537.36',
      ].join(' '),
    }
  );

  return view;
}
