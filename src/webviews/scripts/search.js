let isMouseOver = false;
const htmlElm = document.querySelector('html');

htmlElm.addEventListener('mouseenter', () => {
  isMouseOver = true;
});
htmlElm.addEventListener('mouseleave', () => {
  isMouseOver = false;
});

let refreshIntId = setInterval(() => {
  const search = document
    .querySelector('div[aria-label="Timeline: Search timeline"]');

  if (!isMouseOver && search && htmlElm.scrollTop === 0) {
    htmlElm.scrollTop = 300;
    setTimeout(() => {
      htmlElm.scrollTop = 0;
    }, 300);
  }
}, 10 * 60000);
