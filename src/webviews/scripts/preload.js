import { contextBridge, ipcRenderer } from 'electron';

contextBridge.exposeInMainWorld('__deckAPI', {
  closePreview: () => ipcRenderer.send('closePreview'),
});
