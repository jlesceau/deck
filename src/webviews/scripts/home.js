let isMouseOver = false;
const htmlElm = document.querySelector('html');

htmlElm.addEventListener('mouseenter', () => {
  isMouseOver = true;
});
htmlElm.addEventListener('mouseleave', () => {
  isMouseOver = false;
});

let refreshIntId = setInterval(() => {
  const home = document
    .querySelector('div[aria-label="Timeline: Your Home Timeline"]');

  if (!isMouseOver && home && htmlElm.scrollTop === 0) {
    htmlElm.scrollTop = 300;
    setTimeout(() => {
      htmlElm.scrollTop = 0;
    }, 300);
  }
}, 10 * 60000);
