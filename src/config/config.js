import { app } from 'electron';
import { dirname } from 'path';
import { set, cloneDeep } from 'lodash';
import { promises as fs, constants } from 'fs';

class Config {
  constructor() {
    this.documentsPath = `${ app.getPath('documents') }/Deck`;
    this.configPath = app.isPackaged ?
      `${ this.documentsPath }/config.json` :
      `${ this.documentsPath }/configDev.json`;
    this.defaultConfigPath = `${ __dirname }/defaultConfig.json`;

    this.config = {};
  }

  async loadConfig() {
    try {
      await fs.access(this.configPath, constants.W_OK);
    } catch (ex) {
      const defaultConfig = await fs.readFile(this.defaultConfigPath, 'utf8');

      await fs.mkdir(dirname(this.configPath), { recursive: true });
      await fs.writeFile(this.configPath, defaultConfig);
    }

    this.config = JSON.parse(await fs.readFile(this.configPath, 'utf8'));
  }

  async saveConfig({ key, value }) {
    set(this.config, key, cloneDeep(value));

    await fs.writeFile(
      this.configPath,
      JSON.stringify(this.config, null, '  ')
    );
  }

  getPosition() {
    return cloneDeep(this.config.position);
  }

  getColumns() {
    return cloneDeep(this.config.columns);
  }
}

export default Config;
