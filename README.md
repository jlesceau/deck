# Install dependencies

`npm i`

# Run the dev app

`npm run watch`

# Package the app

`npm run pack`

The .exe will be in the dist directory.

# Config

- Run the app a first time to generate the config file and log into twitter.
- Then open the config file corresponding to the instance you are running
  `Documents/Deck/config.json` or `Documents/Deck/configDev.json`
- Add the columns you need, e.g.:

```
  "columns": [
    {
      "type": "home",
      "url": "https://twitter.com/"
    },
    {
      "type": "list",
      "url": "https://twitter.com/i/lists/6435465435483163558"
    },
    {
      "type": "search",
      "url": "https://twitter.com/search?q=racing"
    },
    {
      "type": "trends",
      "url": "https://twitter.com/i/trends"
    }
  ]
```
